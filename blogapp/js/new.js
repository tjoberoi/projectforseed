// Function to redirect to the home page
function redirectToHome()
{
  window.location = document.getElementById('homelink').href ;
}

// Function to add blog 
function addBlog()
        {
        
        // Creating blog object to be added in the file  
        var blog = new Object();
        var atitle = document.getElementById('addTitle');
        var acategory = document.getElementById('addCategory')
        var acontent = document.getElementById('addContent');
        
        // Applying some validations to avoid invalid data
        if(!atitle.value) alert('Title field is empty');
        else
        {   
        if(!acategory.value) alert('Category field is empty');
        else
        {
        if(!acontent.value) alert('Content field is empty');
        else
        {
             // Getting details of blog from html and adding it in object
             blog.id = null;  
             blog.title = atitle.value;
             blog.category = acategory.value;
             blog.content = acontent.value;
        
        // Fetch api post request to add the blog in the file
        fetch('http://localhost:3000/add',
        {
        method: 'POST',
        body: JSON.stringify(blog),
        headers: {'Content-type': 'application/json; charset=UTF-8'
        }
        }).then(response=> response.json());
        
        // Returning to the home page after adding blog
        redirectToHome(); 
        }  
        }
       } 
      }

      // Function to cancel add operation and return to home page
      function hideAdd()
      {
        redirectToHome();
      }