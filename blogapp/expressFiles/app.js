// Getting all the required modules
const express = require('express');
const path = require('path');
const file = require('fs');
const cors = require('cors');
const app = express();
const port = 3000;

// Using modules that are going to be used in express
app.use(cors());
app.use(express.json());
app.use(express.static(__dirname + '/..'));

// Id to store the id to be deleted/edited
var id;

// Array to store the js objects of blogs 
var blogsCollection = [];

// Path to store json representation of blog objects in file (store.json)
const filePath = path.join(__dirname, "store.json");

// Route for the home (to show main.html)
app.get("/", (req, res) => {
    console.log(blogsCollection);
    res.send(blogsCollection);
    //res.sendFile(path.join(__dirname + "/../html/main.html"));
});

// Route to load list of blogs
app.get("/loadblogs", (req, res) => {
    res.send(blogsCollection);
});

// Route to show edit.html 
app.get("/showedit", (req, res) => {
    id = req.query.id;
    res.sendFile(path.join(__dirname + "/../html/edit.html"));
});

// Route to show new.html where new blogs can be added
app.get("/showadd", (req, res) => {
    res.sendFile(path.join(__dirname + "/../html/new.html"));
});

// Route to show a specific blog with the help of id
app.get("/showblog", (req, res) => {
    console.log("request for show blog arrived");
    
    var n = req.query.id;
    // Traversing the array and returning the matched object
    for (var m in blogsCollection) {
        if (blogsCollection[m].id == n) {
            res.send(blogsCollection[m]);
        }
    }
});

// Route to show delete.html to ask for confirmation to delete
app.get("/showdelete", (req, res) => {
    id = req.query.id;
    res.sendFile(path.join(__dirname + "/../html/delete.html"));
});

// Route (Post) to add a specific blog
app.post("/add", (req, res) => {

    console.log("add called");
    // Getting blog content from request body
    var n = req.body;

    // Giving id to new blog object
    n.id = blogsCollection[0].idCount;

    // Adding js object of blog in blogsCollection array
    blogsCollection.push(n);
    blogsCollection[0].idCount++;

    // Writing json representation of js object in file
    file.writeFile(filePath, JSON.stringify(blogsCollection, null, 2), (err) => {
        if (err) console.log(err);
    });
});

// Route to delete the specific blog object
app.delete("/delete", (req, res) => {
    console.log("req for delete arrived");
    var n = req.query.id;
    console.log("value of id " + n);
    // Removing the specific blog from the array
    for (var m in blogsCollection) {
        if (blogsCollection[m].id == n) blogsCollection.splice(m, 1);
    }

    // Writing the final blogsCollection array to the file
    file.writeFile(filePath, JSON.stringify(blogsCollection, null, 2), (err) => {
        if (err) console.log(err);
    });
    res.send("sucess");
});

// Route to edit the specific blog
app.put("/edit", (req, res) => {
    var data = req.body;
    var n = req.body.id;
    console.log("request for edit arrived");
    // Traversing the entire array and updating the content of the matched blog
    for (var m in blogsCollection) {
        if (blogsCollection[m].id == n) {
            blogsCollection[m].title = data.title;
            blogsCollection[m].category = data.category;
            blogsCollection[m].content = data.content;
            break;
        }
    }

    // Writing the blogsCollection array after updation of the blog
    file.writeFile(filePath, JSON.stringify(blogsCollection, null, 2), (err) => {
        if (err) console.log(err);
    });
});

// Starting server at the given port
const server = app.listen(port, () => {
    console.log(`Listening on port number ${port}`);
    try {
        // If store.json exists loading its content and adding it to the array
        if (file.existsSync(filePath)) {
            file.readFile(filePath, 'utf-8', (err, data) => {
                if (err) throw err;
                var blogs = JSON.parse(data.toString());
                for (var v in blogs) {
                    blogsCollection.push(blogs[v]);
                }
            });
        } else {
            // If store.json does not exists adding idCount 1 to array 
            // and adding array to the newly createdstore.json
            blogsCollection.push({ idCount: 1 })
            file.appendFileSync(filePath, JSON.stringify(blogsCollection, null, 2), (err) => {
                if (err) console.log(err);
            });
        }
    } catch (err) {
        console.error(err);
    }
});