// Id of the blog to be edited
var editId;

// Function to redirect to home
function redirectToHome()
{
  window.location = document.getElementById('homelink').href;
}

// Function to show content of the blog to be edited
function showEditBlog()
{
  
  // Fetch API get request for the editing blog 
  fetch('http://localhost:3000/showblog',
        {
        method: 'GET',
        headers: {'Content-type': 'application/json; charset=UTF-8'
        }
        }).then(response=> response.json())
        .then(json => {
          
          // Adding content got from the back part
          editId = json.id;
          editTitle.value = json.title;
          editCategory.value = json.category;
          editContent.value = json.content;
        });
}

// Function to edit the content of the blog 
function editBlog()
        {
        
        // Creating a new object and adding properties
        var blog = new Object();
        etitle = document.getElementById('editTitle');
        ecategory = document.getElementById('editCategory')
        econtent = document.getElementById('editContent');
        
        // Applying validations
        if(!etitle.value) alert('Title field is empty');
        else
        {              
        if(!ecategory.value) alert('Category field is empty');
        else
        {        
        if(!econtent.value) alert('Content field is empty');
        else
        {       
            var titleValue = etitle.value;
            var categoryValue = ecategory.value;
            var contentValue = econtent.value;

             blog.id = editId; 
             blog.title = etitle.value;
             blog.category = ecategory.value;
             blog.content = econtent.value;
        
        // Fetch put request to update the information of specific blog
        fetch('http://localhost:3000/edit',
        {
        method: 'PUT',
        body: JSON.stringify(blog),
        headers: {'Content-type': 'application/json; charset=UTF-8'
        }
        });
        
        // Returning to home page after updating the blog
        redirectToHome();
        }  
        }
       }
      }

// Function to cancel edit operation
function hideEdit()
{
  redirectToHome();
}