// Id to be deleted
var deleteId;

// Function to redirect to home page (main.html)
function redirectToHome()
{
  window.location = document.getElementById('homelink').href;
}

// Function to show the content of the blog to be deleted
function showDeleteBlog()
{
  
  // Fetch API for making get request
  fetch('http://localhost:3000/showblog',
        {
        method: 'GET',
        headers: {'Content-type': 'application/json; charset=UTF-8'
        }
        }).then(response=> response.json())
        .then(json => {

          // Inserting the data got from the backend (node server)
          deleteId = json.id;
          deleteTitle.value = json.title;
          deleteCategory.value = json.category;
          deleteContent.value = json.content;
        });
}

// Function to finally remove the blog
function deleteBlog()
{
        // Fetch API used to make delete request 
        fetch('http://localhost:3000/delete',
        {
        method: 'DELETE',
        headers: {'Content-type': 'application/json; charset=UTF-8'
        }
        }).then(response=> response.json())
        .then(json => console.log(json));
        redirectToHome();
}

// Funtion cancel delete operation by redirecting to home
function hideDelete()
{
  redirectToHome();
}