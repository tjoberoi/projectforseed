// variable to check whether it is firstTime page is loading 
var firstTime = 1;
        
        // Function to get the list of blogs and show their content
        function initialize()
        {
            
            // Fetch get request to get all the blogs form back end
            fetch('http://localhost:3000/loadblogs',
            {
            method: 'GET',
            headers: {'Content-type': 'application/json; charset=UTF-8'
            }
            }).then(response => response.json())
            .then(json =>{
                
                // Inserting blog objects as row in table
                tab = document.getElementById('blogs');
                g=0;
                for(var i=1 ; i<json.length ; i++)
                {
                  if(g==0)
                  {
                    tab.deleteRow(0);
                    g=1;
                    firstTime = 0;
                  }
                
                  // Creating row to be added in table of blogs
                  var tr =document.createElement('tr');
                  tr.classList.add('table-warning','data-toggle','data-target','accordion-toggle','data-parent','panel','panel-default');
                  tr.setAttribute('data-toggle','collapse');
                  tr.setAttribute('data-target', '#row'+json[i].id);
                  tr.setAttribute('id','f'+json[i].id);
                  tr.setAttribute('data-parent', '#blogs');
              
                  // Creating title of the blog
                  title =document.createElement('td');
                  title.setAttribute('id','r'+json[i].id);
                  title.innerHTML = json[i].title;
                  title.classList.add('font-weight-bold');
                  tr.append(title);
                  
                  // Creating tr for the category and content of the blog
                  rem = document.createElement('tr');
                  rem.classList.add('table-warning','p-3','collapse','panel-collapse','accordion-body');
                  rem.setAttribute('id','row'+json[i].id);

                  hcell = document.createElement('td');
              
                  hdiv = document.createElement('div');
                  hdiv.classList.add('p-3');
                  
                  // Creating label for the category
                  label = document.createElement('Label');
                  label.innerHTML = 'Category : ';
                  label.classList.add('font-weight-bold','mr-1');
            
                  cat = document.createElement('span');
                  cat.setAttribute('id', 'cat'+json[i].id);
                  cat.innerHTML = json[i].category;
                  
                  hdiv.appendChild(label);
                  hdiv.appendChild(cat);
                  hdiv.append(document.createElement('br'));
                  
                  // Creating textarea to take content of the blog
                  con =document.createElement('div');
                  context = document.createElement('textarea');
                  context.cols = 120;
                  context.readOnly = true;
                  context.setAttribute('id','con'+json[i].id);
                  context.innerHTML = json[i].content;
                  
                  // Creating label for the content of the blog
                  conLabel = document.createElement('Label');
                  conLabel.innerHTML = 'Content : ' ;
                  conLabel.classList.add('font-weight-bold');
                  
                  con.appendChild(conLabel);
                  con.appendChild(document.createElement('br'));
                  con.appendChild(context);
                  
                  bttnDiv = document.createElement('div');
                  bttnDiv.classList.add('d-flex','float-right');
                  
                  // Creating anchor to move to the delete page
                  deleteAnchor = document.createElement('a');
                  deleteAnchor.href = 'http://localhost:3000/showdelete?id=' + json[i].id;
                  deleteButton = document.createElement('button');
                  deleteButton.type = 'button';
                  deleteButton.classList.add('btn-sm','btn-outline-dark');
                  deleteButton.innerHTML = 'Delete';
                  deleteButton.id = 'd'+json[i].id;
                  deleteAnchor.appendChild(deleteButton);
                  
                  // Creating anchor to move to the edit page
                  editAnchor = document.createElement('a');
                  editAnchor.href = 'http://localhost:3000/showedit?id=' + json[i].id;
                  editButton = document.createElement('button');
                  editButton.type = 'button';
                  editButton.id = 'e'+ json[i].id;
                  editButton.classList.add('btn-sm','btn-outline-dark','mr-1');
                  editButton.innerHTML = 'Edit';
                  editAnchor.appendChild(editButton);
                  
                  // Adding edit and delete anchor in bttnDiv
                  bttnDiv.appendChild(editAnchor);
                  bttnDiv.appendChild(deleteAnchor);                  
                  con.appendChild(bttnDiv);

                  hdiv.append(con);
                  hcell.append(hdiv);
                  
                  rem.append(hcell);

                  // Adding title, category and content as tr in table
                  tab.append(tr);
                  tab.append(rem);
                }
            }); 
        }